import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Vector <your.name at your.org>
 */
public class DistribuitedCalendarClient {

 
    
    /**
     * @mongod --dbpath D:\MyLearn\DistributedCalenderMongoDB\MongoDB\Server\data
     */
    public static void main(String[] args) throws RemoteException {
        
        Event initial = new Event(1,2,3,"Test", "Test #1", "Desc #1");
        Manager remoteObj = initial;
        
        User userObj = new User("g", "g", "g");
        UserManager remoteObj2 = userObj;
        UserManager remoteObj3 = Login.getInstance();
       
        
        
         Manager stub = (Manager) UnicastRemoteObject.exportObject(remoteObj, 0);
         UserManager stub2 = (UserManager) UnicastRemoteObject.exportObject(remoteObj2, 0);
         UserManager stub3 = (UserManager) UnicastRemoteObject.exportObject(remoteObj3, 0);
        
         
         
         
         Registry registry;
          try {

                registry = LocateRegistry.createRegistry(1099);
            } catch (Exception e) {
                registry = LocateRegistry.getRegistry();
            }
          registry.rebind("Event", stub);
          registry.rebind("User", stub2);
          registry.rebind("Login", stub3);
          
          
          
           while (true) {
                // Todo
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
