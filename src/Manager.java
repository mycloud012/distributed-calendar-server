import java.rmi.RemoteException;
import java.util.Map;


public interface Manager extends java.rmi.Remote {
        public void createEvent(EventShared obj, Usershared user) throws RemoteException;
        public void updateEvent(EventShared obj, Usershared user, String eventTitle) throws RemoteException;
        public void deleteEvent(EventShared obj, Usershared user) throws RemoteException;
        public boolean shareEvent(EventShared obj, String userName) throws RemoteException;
        public Map<String, String> getWordsOccuredInDay(int day, int month) throws RemoteException;
        public Map<String, String> getWordsOccuredInHour(int hour) throws RemoteException;
}