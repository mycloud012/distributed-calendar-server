
import com.mongodb.*;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vector <your.name at your.org>
 */
public class InsertDriver {

    // Singleton pattern
    private InsertDriver()
    {
        
    }
   private static DB dbInstance = null;

    public synchronized static DB InitializeDB()  throws MongoException{
        if(dbInstance == null)
        {
           dbInstance = (new MongoClient("localhost", 27017)).getDB("calender");
            return dbInstance;
        }
        return dbInstance;
    }
    
    /*public static void Insert(String collectionName, String[] args)
    {
        InitializeDB();
        DBCollection dbCollection = db.getCollection("collectionName");
        BasicDBObject basicObject = new BasicDBObject();
        for(int i=0; i<args.length; i++)
        {
            basicObject.put(collectionName, args[i]);
        }
    }*/
    
    

    public static void main(String[] args) {
        DB db = (new MongoClient("localhost", 27017)).getDB("testDB");
        DBCollection dbCollection = db.getCollection("Channel");
        BasicDBObject basicObject = new BasicDBObject();
        basicObject.put("name", "Mohamed");
        basicObject.put("subscriptions", 5000);
        dbCollection.insert(basicObject);
    }
}
