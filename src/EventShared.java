
import java.io.Serializable;

public class EventShared implements Serializable {

    // Event properites
    public int month;
    public int day;
    public int hour;
    public String author;
    public String title;
    public String description;

    EventShared( int month, int day, int hour, String author, String title, String description)
    {
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.author = author;
        this.title = title;
        this.description = description;
    }
}