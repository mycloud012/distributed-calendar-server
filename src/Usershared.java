
import java.io.Serializable;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vector <your.name at your.org>
 */
public class Usershared  implements Serializable{

    private String password;
    private String email;
    private String userName;

    Usershared(String userName, String password, String email) {
        this.userName = userName;
        this.password = password;
        this.email = email;
    }
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public String getPassword()
    {
        return this.password;
    }
    
    public String getEmail()
    {
        return email;
    }
}
