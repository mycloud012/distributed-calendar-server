import java.rmi.RemoteException;
import java.util.ArrayList;


public interface UserManager extends java.rmi.Remote{
    
        public boolean create(Usershared user) throws RemoteException;
        public Usershared authorized(Usershared user) throws RemoteException;
        public ArrayList<EventShared> getEvents(Usershared user) throws RemoteException;
        public ArrayList<EventShared> getSharedEvents(Usershared user) throws RemoteException;
        
}
