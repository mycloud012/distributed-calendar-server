/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mongodb.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Vector <your.name at your.org>
 */
public final class Event implements Manager {

    // Event properites
    private int month;
    private int day;
    private int hour;
    private String author;
    private String title;
    private String description;

    Event(int month, int day, int hour, String author, String title, String description) {
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.author = author;
        this.title = title;
        this.description = description;
        getEventCollection();
    }
    public static DBCollection dbCollection = null;

    public void getEventCollection() {
        if (dbCollection == null) {
            DB dataBase = InsertDriver.InitializeDB();
            dbCollection = dataBase.getCollection("Events");
            System.out.println("getEventCollection");
        }
    }

    @Override
    public synchronized void createEvent(EventShared obj, Usershared user) {
        System.out.println("createEvent()");
        System.out.println("Object: " + obj.title);
        ArrayList<String> authorsList = new ArrayList< String>();
        if (!eventExists(obj)) {
            System.out.println("Inserting into DB");
            BasicDBObject DBObject = new BasicDBObject();
            DBObject.put("month", obj.month);
            DBObject.put("day", obj.day);
            DBObject.put("hour", obj.hour);
            DBObject.put("authorID", user.getUserName());
            DBObject.put("title", obj.title);
            DBObject.put("description", obj.description);
            DBObject.put("sharedAuthors", authorsList); // We will append an ArrayList here 
            dbCollection.insert(DBObject);
        }
    }

    @Override
    public synchronized void deleteEvent(EventShared obj, Usershared user) {
        // Check if either user owns the event or even the event exists
        if (!userOwnsEvent(obj, user) || !eventExists(obj)) {
            System.out.println("either user does not own event, or it does not exists.");
            return;
        }

        System.out.println("post with authorID: " + obj.author + " is removed by userID: " + user.getUserName());
        DBObject query = new BasicDBObject();
        query.put("title", obj.title);
        dbCollection.remove(query);
    }

    // checks if a certain event is shared one or belongs to the user
    public boolean eventShared(EventShared event, Usershared user) {
        ArrayList<String> sharedEvents = new ArrayList<String>();
        DBObject query = new BasicDBObject();
        query.put("title", event.title);
        DBCursor cursor = dbCollection.find(query);

        try {
            if (cursor.hasNext()) {
                DBObject doc = cursor.next();
                sharedEvents = (ArrayList) doc.get("sharedAuthors");
            }

            // Search into each document ArrayList for the user id
            // If found, create an object of that event and add it to the Events list
            for (String author : sharedEvents) {
                System.out.println("Entering the loop");
                if (author.equals(user.getUserName())) {
                    return true;
                }
            }


        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }
        return false;
    }

    public boolean userOwnsEvent(EventShared event, Usershared user) {
        // 1st Check if the event.authorID equals to the user's or not
        // 2nd Check event authorsShared list, search if the user's ID is there or not
        // Return true in any of above cases if found, otherwise if both failed, return a nighty mighty false
        if (event.author == null ? user.getUserName() == null : event.author.equals(user.getUserName())) {
            return true;
        }

        ArrayList<String> sharedEvents = new ArrayList<String>();
        DBObject query = new BasicDBObject();
        query.put("title", event.title);
        DBCursor cursor = dbCollection.find(query);

        try {
            if (cursor.hasNext()) {
                DBObject doc = cursor.next();
                sharedEvents = (ArrayList) doc.get("sharedAuthors");
            }

            // Search into each document ArrayList for the user id
            // If found, create an object of that event and add it to the Events list
            for (String author : sharedEvents) {
                System.out.println("Entering the loop");
                if (author.equals(user.getUserName())) {
                    return true;
                }
            }


        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }

        return false;
    }

    // method to check whether the field exists
    public boolean eventExists(EventShared obj) {
        System.out.println("eventExists: " + obj.title);

        DBObject query = new BasicDBObject();
        query.put("title", obj.title);
        System.out.println(query);

        // store the documents in cursor
        DBCursor cursor = dbCollection.find(query);

        // iterate and print the contents of cursor
        try {
            if (cursor.hasNext()) {
                System.out.println(cursor.next());
                System.out.println("Cursor found a record");
                return true;
            }
        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }
        return false;
    }

    @Override
    public synchronized void updateEvent(EventShared obj, Usershared user, String eventTitle) {
        System.out.println("updateEvent() by " + user.getUserName());
        if (!userOwnsEvent(obj, user)) {
            System.out.println("You may not update other user's event");
            return;
        }

        if (eventShared(obj, user)) {
            System.out.println("Event is in the shared list of the user.");
            BasicDBObject newDocument = new BasicDBObject();
            newDocument.append("$set", new BasicDBObject()
                    .append("month", obj.month)
                    .append("day", obj.day)
                    .append("hour", obj.hour)
                    //.append("authorID", obj.author)
                    .append("title", obj.title)
                    .append("description", obj.description));
            BasicDBObject searchQuery = new BasicDBObject().append("title", eventTitle);

            try {
                dbCollection.update(searchQuery, newDocument);
             
            } catch (WriteConcernException e) {
                System.out.println("Exception: " + e);
            }
              System.out.println("A shared event has been updated");

        } else {
            BasicDBObject newDocument = new BasicDBObject();

            newDocument.append("$set", new BasicDBObject()
                    .append("month", obj.month)
                    .append("day", obj.day)
                    .append("hour", obj.hour)
                    .append("authorID", obj.author)
                    .append("title", obj.title)
                    .append("description", obj.description));
            BasicDBObject searchQuery = new BasicDBObject().append("title", eventTitle);

            try {
                dbCollection.update(searchQuery, newDocument);
            } catch (WriteConcernException e) {
                System.out.println("Exception: " + e);
            }
            System.out.println("Event has been updated");
        }
    }
    
    private boolean userExists(String userName)
    {
        DBObject query = new BasicDBObject();
        query.put("userName", userName);
        
             // store the documents in cursor
        DBCursor cursor = User.userDBCollection.find(query);

        // iterate and print the contents of cursor
        try {
            if (cursor.hasNext()) {
                System.out.println(cursor.next());
                System.out.println("User exists");
                return true;
            }
        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }
        return false;
    }
        
    

    @Override
    public synchronized boolean shareEvent(EventShared event, String userName) {
        if(!userExists(userName))
        {
            System.out.println(userName + " does not exists.");
             return false;
        }
           
        // Get reference to the event we want to share
        // Get the currently stored list in the event sharedAuthors
        // Add 1 new user (if he is not there already)
        // append the new list into DB
        DBObject query = new BasicDBObject();
        query.put("title", event.title);

        ArrayList< String> authorsList = new ArrayList< String>();

        DBCursor cursor = dbCollection.find(query);
        try {
            if (cursor.hasNext()) {

                System.out.println("shareEvent title " + event.title);
                DBCursor authorsCusros = dbCollection.find(query, new BasicDBObject("sharedAuthors", true).append("_id", false));
                authorsList = (ArrayList) authorsCusros.next().get("sharedAuthors");
                System.out.println("authorsCusros.next() size: " + authorsList.size());
                if (!authorsList.contains(userName)) {
                    authorsList.add(userName);
                    System.out.println("user shared event");
                }

            }
        } catch (Exception e) {
            System.out.println("shareEvent EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }


        BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$set", new BasicDBObject()
                .append("month", event.month)
                .append("day", event.day)
                .append("hour", event.hour)
                .append("authorID", event.author)
                .append("title", event.title)
                .append("description", event.description)
                .append("sharedAuthors", authorsList));



        BasicDBObject searchQuery = new BasicDBObject().append("title", event.title);
        System.out.println("authorsList size: " + authorsList.size());

        try {
            dbCollection.update(searchQuery, newDocument);
        } catch (WriteConcernException e) {
            System.out.println("Exception: " + e);
        }

        return true;
    }


    @Override 
    public Map<String, String> getWordsOccuredInDay(int day, int month) {
        Map<String, String> words = new HashMap<String, String>();
        DBObject query = new BasicDBObject();
        query.put("day", day);
        query.put("month", month);
        
        String map = "function() { "
                + "    var word = this.title.split(' ').forEach(function(word){"
                + "         emit(word, 1);"
                + "});"
                + "};";

        String reduce = "function(key, values) { "
                + "var sum = 0;"
                + " values.forEach(function(value){"
                + "sum += value;"
                + " });"
                + " return {total: sum};"
                + "}";

        

        MapReduceCommand cmd = new MapReduceCommand(Event.dbCollection, map, reduce,
                null, MapReduceCommand.OutputType.INLINE, query);
        MapReduceOutput out = Event.dbCollection.mapReduce(cmd);

        try {
            for (DBObject o : out.results()) {

                System.out.println(o.toString());
                words.put(o.get("_id").toString(), o.get("value").toString());

            }
        } catch (Exception e) {
            System.out.println("getWordsOccuredInDay EXCEPTION: " + e);
        }
        return words;
    }
    
    
    
    @Override
       public Map<String, String> getWordsOccuredInHour(int hour) {
         Map<String, String> words = new HashMap<String, String>();
        DBObject query = new BasicDBObject();
        query.put("hour", hour);
        
        String map = "function() { "
                + "    var word = this.title.split(' ').forEach(function(word){"
                + "         emit(word, 1);"
                + "});"
                + "};";

        String reduce = "function(key, values) { "
                + "var sum = 0;"
                + " values.forEach(function(value){"
                + "sum += value;"
                + " });"
                + " return {total: sum};"
                + "}";

        

        MapReduceCommand cmd = new MapReduceCommand(Event.dbCollection, map, reduce,
                null, MapReduceCommand.OutputType.INLINE, query);
        MapReduceOutput out = Event.dbCollection.mapReduce(cmd);

        try {
            for (DBObject o : out.results()) {

                System.out.println(o.get("value").toString());
                words.put(o.get("_id").toString(), o.get("value").toString());

            }
        } catch (Exception e) {
            System.out.println("getWordsOccuredInHour EXCEPTION: " + e);
        }
        return words;
    }
    
    
    
    
    
    
}