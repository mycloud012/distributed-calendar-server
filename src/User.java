import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.rmi.RemoteException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vector <your.name at your.org>
 */
public class User implements UserManager {

    private String password;
    private String email;
    private String userName;
    public static DBCollection userDBCollection = null;

    User(String userName, String password, String email) {
        this.userName = userName;
        this.password = password;
        this.email = email;
        getUserColletcion();
    }

    private void getUserColletcion() {
        if (userDBCollection == null) {
            DB dataBase = InsertDriver.InitializeDB();
            userDBCollection = dataBase.getCollection("Users");
        }
    }

    @Override
    public synchronized boolean create(Usershared user) {
        if (!userExists(user)) {
            System.out.println("Creating user with name " + user.getUserName());
            BasicDBObject DBObject = new BasicDBObject();
            DBObject.put("userName", user.getUserName());
            DBObject.put("password", user.getPassword());
            DBObject.put("email", user.getEmail());
            userDBCollection.insert(DBObject);
            return true;
        }
        return false;
    }

    public String getPassword() 
    {
        return this.password;
    }

    private boolean userExists(Usershared obj) {

        DBObject query = new BasicDBObject();
        query.put("userName", obj.getUserName());
        System.out.println(query);

        // store the documents in cursor
        DBCursor cursor = userDBCollection.find(query);

        // iterate and print the contents of cursor
        try {
            if (cursor.hasNext()) {
                System.out.println(cursor.next());
                System.out.println("User already exists");
                return true;
            }
        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }
        return false;
    }

    public String getUserName() {
        return this.userName;
    }

    @Override
    public ArrayList<EventShared> getEvents(Usershared user) {
        // Lookup all the events which has the userName
        // Put them into a list of Event and return that list
        System.out.println("getEvents called by " + user.getUserName());
        ArrayList<EventShared> userEvents = new ArrayList<EventShared>();

        BasicDBObject basicObject = new BasicDBObject();
        basicObject.put("authorID", user.getUserName());
        DBCursor dbCursor = Event.dbCollection.find(basicObject);


        try {
            while (dbCursor.hasNext()) {
                System.out.println("getEvents found records!");
                
                 DBObject doc = dbCursor.next();
                 //System.out.println("title: " +  doc.get("title").toString());
                EventShared event = new EventShared(
                        // So many parses ;/
                        Integer.parseInt(doc.get("month").toString()),
                        Integer.parseInt(doc.get("day").toString()),
                        Integer.parseInt(doc.get("hour").toString()),
                        doc.get("authorID").toString(),
                        doc.get("title").toString(),
                        doc.get("description").toString());
                userEvents.add(event);
               // System.out.println("LIST SIZE: " + userEvents.size());
            }
        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            dbCursor.close();
        }

         //System.out.println("b4 return list size: " + userEvents.size());
        return userEvents;
    }
    
    
    @Override
        public ArrayList<EventShared> getSharedEvents(Usershared user) {
        // Lookup all the events which has the userName
        // Put them into a list of Event and return that list
        System.out.println("getSharedEvents called by " + user.getUserName());
        
        ArrayList<EventShared> userEvents = new ArrayList<EventShared>();
        ArrayList<String> sharedEvents = new ArrayList<String>();
        
        DBCursor dbCursor = Event.dbCollection.find();

        try {
            while (dbCursor.hasNext()) {
                //System.out.println("getSharedEvents found " + dbCursor.count() + " records!"); 
                 DBObject doc = dbCursor.next();
                 //System.out.println(doc.containsField("sharedAuthors"));
                 //System.out.println("got: " + doc);
                 sharedEvents= (ArrayList)doc.get("sharedAuthors");
               
                 //System.out.println("Got the lis of size " + sharedEvents.size());
                 
                 // Search into each document ArrayList for the user id
                 // If found, create an object of that event and add it to the Events list
                 for(String author  : sharedEvents)
                 {
                     if(author.equals(user.getUserName()))
                     {
                        EventShared event = new EventShared(
                        Integer.parseInt(doc.get("month").toString()),
                        Integer.parseInt(doc.get("day").toString()),
                        Integer.parseInt(doc.get("hour").toString()),
                        doc.get("authorID").toString(),
                        doc.get("title").toString(),
                        doc.get("description").toString());
                      
                        userEvents.add(event);
                     }    
                 }
                //System.out.println("LIST SIZE: " + userEvents.size());
            }
        } catch (Exception e) {
            System.out.println("getSharedEvents EXCEPTION: " + e);
        } finally {
            //close the cursor
            dbCursor.close();
        }
        return userEvents;
    }
    
    
    

    @Override
    public Usershared authorized(Usershared user) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
