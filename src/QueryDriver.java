

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class QueryDriver {
    
     public static void main(String[] args) {
         
       DB db = (new MongoClient("localhost", 27017)).getDB("testDB");
       DBCollection dbCollection = db.getCollection("Channel");
       BasicDBObject basicObject = new BasicDBObject();
       basicObject.put("name", "Mohamed");
       DBCursor dbCursor = dbCollection.find(basicObject);
       
       if(dbCursor.hasNext())
       {
           System.out.println("True");
           System.out.println(dbCursor.next());
          
       }
    }
    
}
