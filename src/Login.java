
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.rmi.RemoteException;
import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 */
public class Login implements UserManager {

    private Login() {
    }
    private static Login instance = null;

    // Thread safe Singleton
    public static synchronized Login getInstance() {
        if (instance == null) {
            instance = new Login();
            return instance;
        }

        return instance;
    }

    // In Login form we do something like 
    // if(login.authorized(user) != null) {Login him in, otherwise either name or pass is wrong}
    @Override
    public Usershared authorized(Usershared user) {

        System.out.println("authorized()");
        DBObject query = new BasicDBObject();
        query.put("password", user.getPassword());
        query.put("userName", user.getUserName());

        DBCursor cursor = User.userDBCollection.find(query);
        try {
            if (cursor.hasNext()) {
                System.out.println("Login Successs");
                DBCursor emailCurosr = User.userDBCollection.find(query, new BasicDBObject("email", true).append("_id", false));
                String email = emailCurosr.next().get("email").toString();

                System.out.println("User Email: " + email);
                // Login success user name and password, simply retreive the email and create the user object
                Usershared obj = new Usershared(user.getUserName(), user.getPassword(), email);
                return obj;
            }
        } catch (Exception e) {
            System.out.println("EXCEPTION: " + e);
        } finally {
            //close the cursor
            cursor.close();
        }
        return null;

    }

    @Override
    public boolean create(Usershared user) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<EventShared> getEvents(Usershared user) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<EventShared> getSharedEvents(Usershared user) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
